import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2'


interface ContactForm {
  "email": string;
  "receive": boolean;
  "country": string;
  "comment": string;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  model = { 
    email: '',
    receive: false,
    country: ''
    
  }

  constructor() { }

  ngOnInit(): void {
    
  }
  onSubmit(form: NgForm): void {

    console.log('Form values', form);
    Swal.fire({
        title: '¡Perfecto, te avisaremos la primera!',
         icon: 'success'
        })
  }

}
function reload() {
  throw new Error('Function not implemented.');
}

